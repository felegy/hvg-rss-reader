Feladat: RSS feed olvas� �s offline-browser
-------------------------------------------

K�sz�tsen olyan C# webes alkalmaz�st, amelynek meg lehet adni RSS 2.0 feed-eket (ilyenek mondjuk a Google News feed-ek), ezeket elteszi adatb�zisba, �s gombnyom�sra let�lti aktu�lis v�ltozatukat. Megn�zi a v�ltoz�sokat, az �j elemek (item) metaadatait (title, line, description,�) elteszi adatb�zisba, valamint az elemn�l megadott linkhez tartoz� oldalt let�lti, �s szint�n elteszi adatb�zisba.

Ehhez m�g k�sz�tsen olyan megjelen�t�t, amin ki lehet v�lasztani egy m�r elt�rolt feed-et, a kiv�laszt�s ut�n ki lehet v�lasztani a feed egy elem�t (item), �s az adott elemhez megmutatja az eltett metaadatokat, illetve megmutatja a teljes dokumentumot, a let�lt�tt �s eltett offline tartalmat (az oldal esetleges tov�bbi hivatkoz�saival � az oldalon megjelen� k�pek, egy�b elemek - nem kell foglalkozni).

Azokat az elemeket, amelyeket a felhaszn�l� kiv�laszt megjelen�t�sre, jel�lje meg, �s legyenek el�rhet�k egy k�l�n kateg�ria alatt (Olvasott cikkek).

A feladat megold�s�hoz a Microsoft Visual Web Developer Express vagy Visual Studio 2010 rendszert lehet haszn�lni. Amennyiben sz�ks�ges, a fejleszt�i k�rnyezethez tudunk telep�t� k�szletet biztos�tani.

Linkek:   

[Microsoft Visual Web Developer](http://msdn.microsoft.com/vstudio/express/vwd/)  

[RSS 2.0 specification](http://blogs.law.harvard.edu/tech/rss)  

[Google News feed-ek](http://news.google.com/intl/en_us/news_feed_terms.html)